.. _introdeploy:

============
Introduction
============

This documentation was made to facilitate employing ChessBot on Amazon AWS. The deployment could be also local as well as on any other cloud service provider.


Goal
====

At the end of this documentation, the user will be able to launch an Amazon EC2 instance and easily deploy ChessBot.


.. admonition:: Prerequisites!
   
   * Have an AWS account.
   * Know the basics of AWS.
   * know the basics of GNU/Linux.
