Utils module
============

The Utils module handles most of the basic move-by-move algorithms.


.. list-table:: Utils functions.
   :widths: 25 25 50
   :header-rows: 1

   * - Function name
     - Arguments
     - Description
   * - initialize_board
     - None
     - Returns a list of pieces in the standard start position.
   * - get_board
     - board: list
  
       white: bool
       
       id: int
  
       red_square = False
  
       coord0 = False
  
       coord1 = False
     
     - Creates a png file of the board with the pieces on, in the main directory.
       Returns nothing.
     
   * - parse_command
     - command: str
     - Returns a list of integers representing either the **start and end position** or **flags** for forfeit, draw proposal or castle moves, referred to in the command.
       
       [-4, -4, -4, -4] for long castle
       
       [-3, -3, -3, -3] for short castle
       
       [-2, -2, -2, -2] for draw proposal
       
       [-1, -1, -1, -1] for forfeit
       
       [x0, y0, x1, y1] with start and end positions of the pieces in the format of (x0, y0) moves to (x1, y1)
       
       Returns False if invalid.
     
   * - letter2numer
     - Letter: str
     - Returns integer that represents the letter passed as argument.
   
   * - number2letter
     - Number: int
     - Returns string that represents the number passed as argument.
   
   * - movePiece
     - x0: int
       
       y0: int
       
       x1: int
       
       y1: int
       
       board: list
       
       white: bool
       
       pgn: PGN
     
     - Attempts to move the piece in the (x0, y0) coordinate to the (x1, y1) coordinate.
       
       Returns:
       
       ‘moved’ if successful,
       
       ‘invalid’ if invalid move.
       
       ‘wrong color’ if piece specified isn’t of the right color.
       
       ‘not found’ if piece not found.
     
   * - check_check
     - board: list     
     - Checks if the arrangement of pieces passed as argument has the kings checked or not.
       
       Returns a list of flags:
       
       [flag1, flag2, flag3]
       
       Flag1: tuple of the white king position, if he’s being checked. It is set to False otherwise.
       
       Flag2: tuple of the black king position, if he’s being checked. It is set to False otherwise.
       
       Flag3: string containing “white_check”, “black_check” or “both_check”, depending on the current situation. It is set to False if there are no checks occurring. 
   
   * - check_stalemate
     - board: list
       
       white: bool
     
     - Checks if there are any moves the player in turn can do.
       
       Returns True if it is stalemate, False if it isn’t.
     
   * - print_board
     - board: list
     - Prints the board in the console, used for debugging.
       
       Returns nothing.
     
   * - check_attacks
     - board: list
       
       squares: list
       
       white: bool
     
     - Checks for attacks in the 3 squares the king will have to go through in a castle.
       
       Returns True if one of the squares are being attacked.
       
       False if they’re clear.
     
   * - short_castle
     - board: list
       
       white: bool
     
     - Attempted to short castle.
       
       Returns:
       
       ‘moved’ if successful,
       
       ‘invalid’ if move is invalid.
     
   * - long_castle 
     - board: list
       
       white: bool
     
     - Attempted to long castle.
       
       Returns:
       
       ‘moved’ if successful,
       
       ‘invalid’ if move is invalid.




