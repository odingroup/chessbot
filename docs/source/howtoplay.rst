How to play?
============


Start the match
---------------

To challenge someone, one must type the command ``-chess @opponent`` with their opponents username. 

The user being challenged must answer yes or no. If the answer is yes, the bot will post a message with the basic instructions, followed by the board image in the start position. 


.. admonition:: First to move
   
   One of the users will randomly be chosen to start as white.

**Example:**

.. only:: html

   .. figure:: /images/exemplo1.gif
      :scale: 125

      *The bot will wait 24 hours for the player in turn to make their move.*

Make moves
----------

Regular moves
+++++++++++++

To make a move the player must send a message with the coordinates of the square of the piece they want to move and the coordinates of the square they want that piece to be moved to.

.. admonition:: Examples

   * At the start of the match, to move the pawn in front of the queen (from d2) 2 squares forward (to d4), the user must send ``d2d4``. 
   * To move the knight from g1 to f3, the user must send ``g1f3``, and so on.
  
Chess bot then recognizes which piece is in the first coordinate (if any) and checks if it can move to the second coordinate. 
If the move is within the rules Chess bot will update the board and post the flipped version of it, so that the other player can make their move.

If the move is not valid, Chess bot will not update the board. It will instead repost it along with a message informing what the issue was.

**Example:**

.. only:: html

   .. figure:: /images/exemplo2.gif
      :scale: 125

      *Regular moves.*

Chess bot recognizes Check, Checkmate and Stalemate situations. It'll highlight the king being attacked if it's check or end the game if it's Checkmate or Stalemate.

**Example:**

.. only:: html

   .. figure:: /images/checkmate.gif
      :scale: 125

      *User checkmates opponent.*

Castle
++++++

As stated in the initial message, Chess bot also handles castling. To castle, the user must send:

* **0-0** for short castle.
* **0-0-0** for long castle.

If the move is within the rules the bot will update the board accordingly.

**Example:**

.. only:: html

   .. figure:: /images/castle.gif
      :scale: 125

      *User castles.*


Forfeiting and proposing a draw
+++++++++++++++++++++++++++++++

The player in turn can also forfeit or propose a draw.

Commands:

* **-forfeit** will forfeit the game and end the game.
* **-draw** will propose a draw and end the game, should the other player accept it.

**Example:**

.. only:: html

   .. figure:: /images/draw.gif
      :scale: 125

      *Draw proposal.*