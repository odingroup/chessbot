===
FAQ
===

Found a bug?
------------

Feel free to report an issue `here <https://gitlab.com/odingroup/chessbot/-/issues>`__.
