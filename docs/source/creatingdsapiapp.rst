================================
Creating Discord API application
================================

In this section we’ll go over how to make a Discord API application and configure it.

Make new application
--------------------

To make a new application the user must go to the application section of the `discord developer portal <https://discord.com/developers/applications>`__ and click **new application**, as shown below.

.. figure:: /images/deploy1.png
    :scale: 50

Name the application. This isn’t the name of the bot, so feel free to name it as anything.

.. figure:: /images/deploy2.png
    :scale: 100

Add bot to application
----------------------

On the bot section, add a bot instance to the application created.

.. figure:: /images/deploy3.png
    :scale: 50

Here the user must choose the name of the bot and copy the bot token, also known as bot key.


.. figure:: /images/deploy4.png
    :scale: 50


.. danger::

    This token is the bot’s access key, do **not** share it.


Get invite link
---------------

To get an invite link, the user must go to the OAuth2 section and tick the bot box.

.. figure:: /images/deploy5.png
    :scale: 50

The bot permission area with all the bot permissions will appear, feel free to mark all the permissions you want the bot to have. 

.. admonition:: Recommendation
    
    For Chess bot to work properly, we recommend marking the **Send Messages**, **Embed Links**, **Attach Files** and **Add Reactions** permissions.

.. figure:: /images/deploy6.png
    :scale: 50

















