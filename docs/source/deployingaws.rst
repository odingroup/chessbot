================
Deploying to AWS
================


Launch an EC2 instance
----------------------

Go to `this link <https://console.aws.amazon.com/?nc2=h_m_mc>`__ and log in your account. After you've logged in, be sure to change your AWS location based on your location and costs.

Open the `EC2 service <https://console.aws.amazon.com/ec2/>`__ , click on **Instances** in the left sidebar and click on **Launch Instance** to set up a new EC2 instance. Now follow the steps below:

#. For the AMI type, select **Ubuntu Server 20.04 LTS** with **64-bit(x86)** architecture.
#. Select an instance type (ChessBot uses very little CPU and RAM for normal Discord use, but if you plan to use it on large servers, you may want to opt for a more robust type) and click **Next**.
#. Configure the details according to your needs or keep the default. Proceed with **Next**.
#. Select a storage size (ChessBot uses no Storage space but you may want to opt for a bigger size if more space is needed in the future or for parallel applications) and click **Next**.
#. Create a **Name** tag for your EC2 instance name according to your preference.
#. Allow **SSH, HTTP and HTTPS** types in the security group configuration. Click **verify and activate**.
#. Review your configuration and confirm.
#. Choose an existing key pair to access your machine remotely with SSH or create a new one.
#. Launch your instance.


Accessing EC2 and lauching ChessBot
-----------------------------------

SSH to your remote EC2 instance:

.. code-block:: bash

   ssh -i <key.pem> ubuntu@<ip_adress>

Clone ChessBot's repository:

.. code-block:: bash

   git clone https://gitlab.com/odingroup/chessbot 

Change directory:

.. code-block:: bash

   cd chessbot 

Install pip:

.. code-block:: bash

   sudo apt install python3-pip

Install ChessBot's dependencies:

.. code-block:: bash

   pip install -r requirements.txt    

Insert your bot key:

.. code-block:: bash

   echo "BOT_KEY=<your_bot_key>" > .env    

Start ChessBot:

.. code-block:: bash

   python3 application.py









