References
==========

* `Discord.py documentation <https://discordpy.readthedocs.io/en/stable/>`__
* `Python documentation <https://docs.python.org/3/>`__
* `Discord documentation <https://discord.com/developers/docs/intro>`__
* `Numpy documentation <https://numpy.org/doc/>`__
* `Amazon EC2 documentation <https://docs.aws.amazon.com/ec2/?id=docs_gateway>`__

