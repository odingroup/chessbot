Flow of the ``-chess`` command
==============================

The flow of the code was divided in 2 basic flow diagrams, the first one for the initial challenge proposed by one of the players, and the second about after the game itself starts.


Challenge prompt
----------------

When a user posts the command ``-chess``, this is the diagram that represents the inner logic of the code in this stage.

.. figure:: /images/diagrama1.png
    :scale: 50
    
As diagram shows, there are 4 ways of the command failing to start a game, those being:

* User didn’t specify who they’re challenging.
* One of the players is already in another game.
* User being challenged took too long to answer.
* User being challenged denied the challenge.

Should the user being challenged accept the challenge in time, the game will start.


Chess match
-----------

When both users are ready to play, the match will start. The diagram below shows the inner logic of the code in this stage.

.. figure:: /images/diagrama2.png
    :scale: 25

As diagram shows, there are 5 ways of the game ending, those being:

* Checkmate
* Stalemate
* Forfeit
* Draw agreement
* Move timeout






