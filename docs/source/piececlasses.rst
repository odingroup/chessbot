Piece classes
=============

Each individual piece is an object of the class that represents it. They contain all the methods related to that specific piece, below we have a class diagram showing their relationship.

.. figure:: images/classes_classdiagram.png
    :scale: 50

**Piece** is the base class for all pieces, it has methods and parameters that are used for all pieces, such as their x and y coordinates, their piece color and the methods *__str__*, *checkForPieceColor* and *move*.

.. attention::

    The move method gets completely overwritten in the Pawn class, due to the pawns unique nature. 
    
    It gets slightly modified in the King and Rook classes, due to castling restrictions.

All classes that inherit from Piece get assigned extra attributes for their specific *letter* and *image* representation, as well as a list of their *possible_moves* and *possible_attacks*.
They’ll also get new methods, those being *getPossibleMove* and *getPossibleAttacks*.

*getPossibleMoves* and *getPossibleAttacks* return the list of possible moves and attacks, respectively, of that specific piece. 
These methods help with the move, check and checkmate algorithms.

