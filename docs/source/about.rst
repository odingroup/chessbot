.. _About:

=====
About
=====

Chess bot was created by **Zan#0002** as a learning experience in the concept of classes and as a challenge to implement chess on Discord through Python.

The documentation and hosting were made as a collaborative project by **Zan#0002**, **bento#6928** and **guibode#3128**.

To contact any of the creators, feel free to message any of them through the discord usernames listed above.

The project
-----------

You can access the project by clicking `here <https://gitlab.com/odingroup/chessbot>`__.