How to invite Chess bot?
========================

Invite Chess bot through `this link <https://discord.com/oauth2/authorize?client_id=869387540862488606&permissions=51264&scope=bot>`__.

After clicking the link above, the user has to choose which server they will be inviting Chess bot to. And upon authorizing the permissions that it will have in that server, Chess bot will join it.

.. figure:: /images/howtoinvite.png
    :width: 418

.. figure:: /images/howtoinvite2.png