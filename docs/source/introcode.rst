.. _introcode:

Introduction
============

Chess Bot allows two players to play chess through a Discord channel. 
One user has to challenge an opponent for a chess match, it will start once the opponent accepts. 

Goal
====

At the end of this documentation, the user must be able to understand the functionality and the logic implemented, as well as know how to add to and modify the code.

.. admonition:: Prerequisites
   
   * Know the `basics of discord <https://support.discord.com/hc/en-us/articles/360045138571-Beginner-s-Guide-to-Discord>`__.
   * Know the `basics of chess <https://www.chess.com/learn-how-to-play-chess>`__.
   * Know Python programming.

