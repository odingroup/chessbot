====================================
Welcome to chessbot's documentation!
====================================

Chess Bot allows two players to play chess through a Discord channel. One user has to challenge an opponent for a chess match, it will start once the opponent accepts.

Goal
----

This documentation aims to cover all essential topics related to:

* Playing chess through the bot. (:ref:`End User Section <introduction>`)
* Modify the bot’s code.  (:ref:`Developer Section <introcode>`)
* Deploy a Discord application on AWS.  (:ref:`Deploy Section <introdeploy>`)

You can find the repository for this application `here <https://gitlab.com/odingroup/chessbot>`__.

For more information about the bot check out the :ref:`About section <About>`.



.. toctree::
   :maxdepth: 5
   :caption: End user
   :hidden:

   Introduction
   invitethebot
   howtoplay


.. toctree::
   :maxdepth: 5
   :caption: Developer
   :hidden:

   introcode
   flowofthechess
   thecode
   piececlasses
   utils



.. toctree::
   :maxdepth: 5
   :caption: Deploy
   :hidden:

   introdeploy
   creatingdsapiapp
   deployingaws


.. toctree::
   :maxdepth: 5
   :caption: Meta
   :hidden:

   faq
   about
   references
