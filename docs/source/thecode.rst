The code
========

This section will go over the code of the main application.py file, it consists mainly of import calls, variable creations, decorators and function calls.

Inside the decorators we have the code for the execution of the command or event being called. 

Requirements
----------------------------

Chess bot requires the following external modules to work properly:

* discord
* python-dotenv
* pillow
* requests

To install all of them run the command:

.. code-block:: bash

    pip install -r requirements.txt


Imports and global variables
----------------------------

.. code-block:: Python
   
    import os
    import asyncio
    import random
    import discord
    from dotenv import load_dotenv
    from discord.ext import commands

    from src import utils
    from src.pgn import PGN

    load_dotenv()
    BOT_KEY = os.getenv("BOT_KEY")
    client = commands.Bot(command_prefix="-")

    MATCH_ID = 0
    MOVE_TIMEOUT = 86400  # 24 hour moves
    VERSION = "a.b"
    START_INSTRUCTIONS = "..."
    
    if __name__ == "__main__":
        client.run(BOT_KEY)

Things to note:

* The *discord*, *os*, *asyncio*, *random* and *dotenv* modules are used for the basic code functionalities. The *requests* and *pillow* modules are used in other parts of the code and are also necessary for the bot to work.

* The *utils* file has many of the methods used in the chess command and is covered in the *Utils module* section.

* The PGN class handles the PGN notation of the moves made.

* The command prefix is set to "-" by ``commands.Bot(command_prefix=”-”)``. This makes it so messages need to start with “-” to be recognized as commands.

* ``client.run(BOT_KEY)`` needs to be at the end of the file, the code following it will not be executed.



Chess command
-------------

As stated in the section *Flow of the -chess command*, the code for the chess command is divided in 2 parts, we’ll cover those separately again.


Command prompt
++++++++++++++


Below we have the code for the first part of the command:

.. code-block:: Python

    @client.command()
    async def chess(ctx, member: discord.Member = None):
        """
        chess command
        """
        if member is None:
            await ctx.send("You need someone to play with")
        else:
            await ctx.send(
                f"{member.mention}, do you want to play Chess? (yes/no)"
            )

            def check1(message):
                return (message.content in ("yes","no") and 
                        message.author == member)

            try:
                msg = await client.wait_for("message", check=check1, timeout=60.0)
            except asyncio.TimeoutError:
                await ctx.message.add_reaction("😴")
                await ctx.send(f"{member.mention} took too long to respond.")
                return

            if msg.content in ("yes", "y"):  # match will start
            . . .
            . . .
            elif msg.content.lower() in ("no", "n"):
                await ctx.send("ok 😔")



.. admonition:: Information


    **ctx** represents the context in which a command is being invoked under.
    It is not created manually and is instead passed around to commands as the first parameter. 
    
    It has several useful methods and attributes that are used in this code, for more information, visit `discord.py documentation <https://discordpy.readthedocs.io/en/stable/ext/commands/api.html?highlight=context#discord.ext.commands.Context”>`__.





Chess match
+++++++++++

Variable initialization
***********************

Now that we know that the match will go through, we have to initialize all the game variables:

.. code-block:: Python

    global MOVE_TIMEOUT                          # time limit for moves
    global MATCH_ID                              # global id tracker
    current_match = MATCH_ID                     # id of this match
    players = [member, ctx.author]               # list of players
    random.shuffle(players)                      # shuffle order
    turn = players[0]                            # select who starts
    pgn = PGN(str(players[0]), str(players[1]))  # initialize pgn
    turn_white = True                            # white starts
    red_square = False                           # check flag
    board = utils.initialize_board()             # board instance
    utils.get_board(board, turn_white, current_match, red_square)
    MATCH_ID += 1

    def check2(message):  # function that checks for move
        return ((message.author.id == turn.id) and
                message.channel == ctx.channel and
                (utils.parse_command(message.content)))
    await ctx.send(START_INSTRUCTIONS)


These are all the variables that are used as flags during the game, check2 is a function that gets passed as an argument for checking if a command is valid or not.

It is important to note that the board instance is a list of piece objects. It gets created in the utils module and will be covered in the *utils module* section.



Game loop
*********

.. code-block:: Python

    # game loop
    while True:
        turn_color = "White" if turn_white else "Black"
        await ctx.send(
            f"{turn.mention}'s turn - {turn_color} moves!",
            file=discord.File(str(current_match) + ".png"),
        )
        try:
            msg = await client.wait_for(
                "message",
                check=check2,
                timeout=MOVE_TIMEOUT)
        except asyncio.TimeoutError:
            await ctx.send("Player took too long to respond")
            winner = "White" if turn_color == "Black" else "Black"
            await end_game(
                current_match,
                ctx,
                result=winner,
                pgn=pgn)
            break
        command = utils.parse_command(msg.content)
        if command:
            [x_0, y_0, x_1, y_1] = command
            if [x_0, y_0, x_1, y_1] == [-2, -2, -2, -2]:
                # propose draw
                other_player = (
                    players[0] if turn == players[1] else players[1])
                await ctx.send(
                    f"**{turn.name}** proposes a draw." +
                    f"{other_player.mention}" +
                    "do you accept? (yes/no)")
                try:
                    msg2 = await client.wait_for("message",
                                                 check=check1,
                                                 timeout=60.0)
                except asyncio.TimeoutError:
                    await msg.add_reaction("😴")
                    continue
                if msg2.content == "yes":
                    await ctx.send("Match ends in a draw! 🙊")
                    await end_game(current_match,
                                    ctx,
                                    result='draw',
                                    pgn=pgn
                                    )
                    break
                if msg2.content == "no":
                    await ctx.send("The match will procede")
            elif [x_0, y_0, x_1, y_1] == [-1, -1, -1, -1]:  # forfeit
                winner = "White" if turn_color == "Black" else "Black"
                await ctx.send(f"{turn_color} forfeits," +
                                f"**{winner}** wins! 🎉")
                await end_game(current_match,
                                ctx,
                                result=winner,
                                pgn=pgn)
                break

            # actual movement
            movimento = utils.move_piece(
                x_0, y_0, x_1, y_1, board, turn_white, pgn)
            if movimento == "moved":
                [position_white, position_black,
                    check] = utils.check_check(board)
                # paint the square red
                red_square = (position_white if check ==
                                "white_check" else position_black)
                # update board image
                utils.get_board(
                    board,
                    not turn_white,
                    current_match,
                    red_square,
                    (x_0, y_0),
                    (x_1, y_1))
                if check in ("white_check", "black_check"):
                    # if king is under attack
                    if utils.check_stalemate(board, not turn_white):
                        await ctx.send(
                            f"Checkmate! {turn_color} wins! 🎉",
                            file=discord.File(
                                str(current_match) + ".png"),
                        )
                        await end_game(current_match,
                                        ctx,
                                        result=turn_color,
                                        pgn=pgn)
                        break
                    await ctx.send("**CHECK!**")
                if utils.check_stalemate(board, not turn_white):
                    await ctx.send(
                        "**Stalemate!** Nobody wins 🤷‍♂️",
                        file=discord.File(str(current_match) + ".png"),
                    )
                    await end_game(current_match,
                                    ctx,
                                    result='draw',
                                    pgn=pgn
                                    )
                    break
                turn = players[0] if turn == players[1] else players[1]
                turn_white = not turn_white
            elif movimento == "wrong color":
                await ctx.send("this piece is not yours")
            elif movimento == "invalid":
                await ctx.send("invalid move")
            elif movimento == "not found":
                await ctx.send("piece not found")



The flow of this code and the logic behind it can be better visualized in the diagram in the *Flow of the -chess command* section.

All functions from the utils module will be covered in the *utils module* section.


Other commands or events
************************

The bot comes with a couple other commands and events, they are used to help maintain the chess functionality and bot itself.

.. code-block:: Python

    @client.event
    async def on_ready():
        print("We have logged in as {0.user}".format(client))
        await client.change_presence(
            activity=discord.Game(name=f"-chess v: {VERSION}"))

The **on_ready** event notifies the host that the bot is logged in and ready to be used.


.. code-block:: Python

    @client.event
    async def on_message(message: discord.Message):
        if message.author == client.user:
            return
        if message.content.lower() in ('-forfeit', '-draw'):
            return
        await client.process_commands(message)



The **on_message** event handles all the messages. It will ignore messages from the bot itself and also ``-forfeit`` and ``-draw`` messages because those are already handled by the main chess command.

After handling these specific cases, it calls client.process_commands(message), which will check for commands in the message and process them as such, should they be one.


.. code-block:: Python

    @client.command()
    async def ping(ctx):
        await ctx.channel.send(f"pong\nversion {VERSION}")


The **-ping** command lets the user know if the bot is ready to be used, as well as the current version of it.


.. code-block:: Python

    async def end_game(current_match,
                    ctx,
                    result=None,
                    pgn=None):
        if pgn is not None:
            pgn.end_game(result)
            await ctx.send("To see an analysis of the game go to " +
                        pgn.get_link_analise())
        if os.path.exists(str(current_match) + ".png"):
            os.remove(str(current_match) + ".png")


The endGame function handles the things related to ending the game. 
It will post a link to the lichess.com analysis of the game that was just played and delete the board image that was stored and posted throughout the match.



