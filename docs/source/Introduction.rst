.. _introduction:

============
Introduction
============

Chess Bot allows two players to play chess through a Discord channel. 
One user has to challenge an opponent for a chess match, it will start once the opponent accepts. 

Goal
====

At the end of this documentation, the user must be capable of adding Chess Bot to their Discord environment and to play chess through it. 

.. admonition:: Prerequisites

   * Have an internet connection.
   * Know the `basics of Discord <https://support.discord.com/hc/en-us/articles/360045138571-Beginner-s-Guide-to-Discord>`__.
   * Know how to `play Chess <https://www.chess.com/learn-how-to-play-chess>`__.