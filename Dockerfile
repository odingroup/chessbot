FROM python:3.9-slim-buster

SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN mkdir app
WORKDIR /app
COPY . .
RUN pip install -r requirements.txt
EXPOSE 443

CMD ["python3", "application.py"]
