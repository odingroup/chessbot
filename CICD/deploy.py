import requests
import sys

TOKEN = sys.argv[1]
URL = 'https://readthedocs.org/api/v3/projects/discordchess/versions/latest/builds/'
HEADERS = {'Authorization': f'token {TOKEN}'}
response = requests.post(URL, headers=HEADERS)
