from os import listdir
from os.path import isfile, join


blacklist = []
with open('./docs/blacklist.txt', 'r') as blfile:
    blacklist0 = blfile.readlines()
    for word in blacklist0:
        blacklist.append(word.rstrip())


mypath = './docs/source/'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for file in onlyfiles:
    if file.endswith('.rst'):
        with open(mypath + file, 'r') as myfile:
            data = myfile.read()
            for word in blacklist:
                if word in data:
                    raise Exception(f'{word} found in {file}')
