"""
rook module
"""

from PIL import Image
from src import piece


class Rook(piece.Piece):
    """
    Rook class
    """

    def __init__(self, x: int, y: int, white: bool) -> None:
        """
        rook init
        """
        super().__init__(x, y, white)
        self.letter = "R"
        self.moves = 0
        if white:
            self.img = Image.open('img/white_rook.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_rook.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get possible moves
        """
        self.possible_moves = []

        i = self.x
        while i < 7:  # checking moves to the right
            i = i + 1
            test = self.check_for_piece_color(board, i, self.y)
            if test == 'empty':
                self.possible_moves.append((i, self.y))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, self.y))
                break

        i = self.x
        while i > 0:  # checking moves to the left
            i = i - 1
            test = self.check_for_piece_color(board, i, self.y)
            if test == 'empty':
                self.possible_moves.append((i, self.y))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, self.y))
                break

        i = self.y
        while i < 7:  # checking moves up
            i = i + 1
            test = self.check_for_piece_color(board, self.x, i)
            if test == 'empty':
                self.possible_moves.append((self.x, i))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((self.x, i))
                break

        i = self.y
        while i > 0:  # checking moves down
            i = i - 1
            test = self.check_for_piece_color(board, self.x, i)
            if test == 'empty':
                self.possible_moves.append((self.x, i))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((self.x, i))
                break
        return self.possible_moves

    def get_possible_attacks(self, board: list) -> list:
        """
        get possible attacks
        """
        return self.get_possible_moves(board)

    def move(self, x_coord: int, y_coord: int, chess_board: list) -> bool:
        """
        move rook
        """
        if super().move(x_coord, y_coord, chess_board):
            self.moves += 1
            return True
        return False
