"""
pawn module
"""

from PIL import Image
from src import piece
from src import utils
from src import queen


class Pawn(piece.Piece):
    """
    Pawn class
    """

    def __init__(self, x_coord: int, y_coord: int, white: bool):
        """
        Pawns init
        """
        super().__init__(x_coord, y_coord, white)
        self.letter = ''
        self.moves = 0
        self.possible_moves = []
        self.possible_attacks = []
        if white:
            self.img = Image.open('img/white_pawn.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_pawn.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get Pawns possible moves
        """
        if self.white:
            self.possible_moves = [(self.x, self.y+1)]  # forward
            if self.moves == 0:
                # goes forward 2 spaces if it hasn't moved yet
                self.possible_moves.append((self.x, self.y+2))
            for i in board:
                if (i.x, i.y) == (self.x, self.y):
                    continue
                if ((i.x, i.y) in self.possible_moves and
                        i.x == self.x and
                        (i.y == self.y + 1 or i.y == self.y + 2)):
                    # if there's a piece where it wants to go,
                    # it can't move there
                    self.possible_moves.remove((i.x, i.y))
                    if (self.x, self.y - 2) in self.possible_moves:
                        self.possible_moves.remove((self.x, self.y + 2))
                if ((i.x == self.x + 1 or i.x == self.x-1) and
                        (i.y == self.y + 1) and
                        not i.white):
                    self.possible_moves.append((i.x, i.y))
                    # move diagonally
                if (isinstance(i, Pawn) and
                        self.y == 4 and
                        self.y == i.y and
                        i.moves == 1 and
                        i.just_moved2):
                    self.possible_moves.append((i.x, i.y+1))
                    # en passant
            return self.possible_moves
        # else
        self.possible_moves = [(self.x, self.y - 1)]
        # forward (down)
        if self.moves == 0:
            # goes forward 2 spaces (down) if it hasn't moved yet
            self.possible_moves.append((self.x, self.y - 2))
        for i in board:
            if (i.x, i.y) == (self.x, self.y):
                pass
            if ((i.x, i.y) in self.possible_moves and
                    i.x == self.x and
                    (i.y == self.y - 1 or i.y == self.y - 2)):
                # if there's a piece where it wants to go (down),
                # it can't move there
                self.possible_moves.remove((i.x, i.y))
                if (self.x, self.y - 2) in self.possible_moves:
                    self.possible_moves.remove((self.x, self.y - 2))
            if ((i.x == self.x + 1 or i.x == self.x - 1)
                    and (i.y == self.y - 1)
                    and i.white):
                self.possible_moves.append((i.x, i.y))
                # move diagonally
            if (isinstance(i, Pawn) and
                    self.y == 3 and
                    self.y == i.y and
                    i.moves == 1 and
                    i.just_moved2):
                self.possible_moves.append((i.x, i.y - 1))
                # en passant
        return self.possible_moves

    def get_possible_attacks(self, _) -> list:
        """
        get Pawns possible attacks
        """
        if self.white:
            return [(self.x - 1, self.y + 1), (self.x + 1, self.y + 1)]
        return [(self.x - 1, self.y - 1), (self.x + 1, self.y - 1)]

    def move(self, x_coord: int, y_coord: int, chess_board: list) -> bool:
        """
        move pawn
        """
        if (x_coord, y_coord) in self.get_possible_moves(chess_board):
            oldx = self.x
            oldy = self.y
            old_piece = False
            for i in chess_board:
                if (i.x, i.y) == (x_coord, y_coord):
                    if i.white is not self.white:
                        chess_board.remove(i)
                        old_piece = i
                if isinstance(i, Pawn):
                    i.just_moved2 = False
            self.x = x_coord
            self.y = y_coord
            [_, _, check] = utils.check_check(chess_board)  # aqui
            # if at the end of x move, there's a check on x king,
            # go back to how it was before
            if (check == 'both_check' or
                    (check == 'white_check' and self.white) or
                    (check == 'black_check' and not self.white)):
                if old_piece:
                    chess_board.append(old_piece)
                self.x = oldx
                self.y = oldy
                return False
            # if it moved to the side,
            # but never removed a piece -> en passant ->
            # remove the piece behind it
            if abs(oldx-self.x) == 1 and not old_piece:
                deltay = 1 if self.white else -1
                for i in chess_board:
                    if (i.x, i.y) == (self.x, self.y-deltay):
                        chess_board.remove(i)
            if abs(self.y - oldy) == 2:
                # just_moved2 flag - for en passant
                self.just_moved2 = True
            if self.y == 7 or self.y == 0:
                # if pawn gets to the end of the board, promote
                chess_board.remove(self)
                chess_board.append(queen.Queen(self.x, self.y, self.white))
            self.moves += 1
            return True
        return False
