"""
knight module
"""

from PIL import Image
from src import piece


class Knight(piece.Piece):
    """
    Knight class
    """

    def __init__(self, x_coord: int, y_coord: int, white: bool) -> None:
        """
        Knights init
        """
        super().__init__(x_coord, y_coord, white)
        self.letter = 'N'
        self.possible_moves = []
        self.possible_attacks = []
        if white:
            self.img = Image.open('img/white_knight.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_knight.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get Knights possible moves
        """
        self.possible_moves = []
        if self.x + 2 < 8:
            if self.y + 1 < 8:
                self.possible_moves.append((self.x + 2, self.y + 1))
            if self.y - 1 >= 0:
                self.possible_moves.append((self.x + 2, self.y - 1))
        if self.x - 2 >= 0:
            if self.y + 1 < 8:
                self.possible_moves.append((self.x - 2, self.y + 1))
            if self.y - 1 >= 0:
                self.possible_moves.append((self.x - 2, self.y - 1))
        if self.y + 2 < 8:
            if self.x + 1 < 8:
                self.possible_moves.append((self.x + 1, self.y + 2))
            if self.x - 1 >= 0:
                self.possible_moves.append((self.x - 1, self.y + 2))
        if self.y - 2 >= 0:
            if self.x + 1 < 8:
                self.possible_moves.append((self.x + 1, self.y - 2))
            if self.x - 1 >= 0:
                self.possible_moves.append((self.x - 1, self.y - 2))

        for i in board:
            if i.white is not self.white:
                continue
            if (i.x, i.y) in self.possible_moves:
                self.possible_moves.remove((i.x, i.y))
        return self.possible_moves

    def get_possible_attacks(self, board: list) -> list:
        """
        get Knights possible moves
        """
        return self.get_possible_moves(board)
