"""
This module has several useful functions that are used in
the Chess functionality.
"""

import copy
from PIL import Image
import src as s


def initialize_board() -> list:
    r"""Returns a list of pieces in the standard start position.

    Parameters
    ----------
    None

    Returns
    -------
    board: list
        List of pieces in the standard start position.

    """
    b_0 = [s.Rook(0, 7, False), s.Knight(1, 7, False),
           s.Bishop(2, 7, False), s.Queen(3, 7, False),
           s.King(4, 7, False), s.Bishop(5, 7, False),
           s.Knight(6, 7, False), s.Rook(7, 7, False),
           s.Pawn(0, 6, False), s.Pawn(1, 6, False),
           s.Pawn(2, 6, False), s.Pawn(3, 6, False),
           s.Pawn(4, 6, False), s.Pawn(5, 6, False),
           s.Pawn(6, 6, False), s.Pawn(7, 6, False),
           s.Pawn(0, 1, True), s.Pawn(1, 1, True),
           s.Pawn(2, 1, True), s.Pawn(3, 1, True),
           s.Pawn(4, 1, True), s.Pawn(5, 1, True),
           s.Pawn(6, 1, True), s.Pawn(7, 1, True),
           s.Rook(0, 0, True), s.Knight(1, 0, True),
           s.Bishop(2, 0, True), s.Queen(3, 0, True),
           s.King(4, 0, True),  s.Bishop(5, 0, True),
           s.Knight(6, 0, True), s.Rook(7, 0, True)
           ]
    return b_0


def get_board(board: list,
              white: bool,
              match_id: int,
              red_square=False,
              coord0=False,
              coord1=False
              ) -> None:
    r"""Creates a png file of the board with the pieces on, in the main directory.

    Parameters
    ----------
    board: list
        list of all pieces.
    white: bool
        turn flag.
    match_id: int
        match id.
    red_square: tuple or False
        tuple of the kings position, if he's being checked.
    coord0: tuple or False
        tuple of the starting coord of the moving piece.
    coord1 = False
        tuple of the finishing coord of the moving piece.

    Returns
    -------
    None.

    """
    if white:
        board_img = Image.open("img/board_w.png").convert('RGBA')
        if red_square:
            square_img = Image.open("img/red_square.png").convert('RGBA')
            board_img.paste(
                square_img,
                (red_square[0] * 50, abs(red_square[1] - 7) * 50),
                square_img)
        if coord0 and coord1 and coord0 != coord1:
            square_img = Image.open("img/path_square.png").convert('RGBA')
            board_img.paste(
                square_img,
                (coord0[0] * 50, abs(coord0[1] - 7) * 50),
                square_img)
            board_img.paste(
                square_img, (coord1[0] * 50, abs(coord1[1] - 7) * 50),
                square_img)
        for piece in board:
            board_img.paste(piece.img,
                            (piece.x * 50, abs(piece.y - 7) * 50),
                            piece.img)
    else:
        board_img = Image.open("img/board_b.png").convert('RGBA')
        if red_square:
            square_img = Image.open("img/red_square.png").convert('RGBA')
            board_img.paste(
                square_img,
                (abs(red_square[0] - 7) * 50, red_square[1] * 50),
                square_img)
        if coord0 and coord1 and coord0 != coord1:
            square_img = Image.open("img/path_square.png").convert('RGBA')
            board_img.paste(
                square_img,
                (abs(coord0[0] - 7) * 50, coord0[1] * 50),
                square_img)
            board_img.paste(
                square_img,
                (abs(coord1[0] - 7) * 50, coord1[1] * 50),
                square_img)
        for piece in board:
            board_img.paste(piece.img,
                            (abs(piece.x - 7) * 50, piece.y * 50),
                            piece.img)
    board_img.save(str(match_id)+'.png')


def parse_command(command: str) -> list:
    r"""Returns a list of integers representing either the start and end
    position or flags for forfeit, draw proposal or castle moves.

    Parameters
    ----------
    command: str
        message sent by the player.

    Returns
    -------
    [-4, -4, -4, -4] for long castle.

    [-3, -3, -3, -3] for short castle.

    [-2, -2, -2, -2] for draw proposal.

    [-1, -1, -1, -1] for forfeit.

    [x_0, y_0, x_1, y_1] with start and end positions of the pieces
    in the format of "(x_0, y_0) moves to (x_1, y_1)".

    Returns False if invalid.
    """
    command = command.lower()
    if command == "0-0-0":
        return [-4, -4, -4, -4]  # long castle code
    if command == "0-0":
        return [-3, -3, -3, -3]  # short castle
    if command == "-draw":
        return [-2, -2, -2, -2]  # draw proposal
    if command == "-forfeit":
        return [-1, -1, -1, -1]  # forfeit
    try:
        x_0 = letter2number(command[0])
        y_0 = int(command[1])-1
        x_1 = letter2number(command[2])
        y_1 = int(command[3])-1
        return [x_0, y_0, x_1, y_1]
    except ValueError:
        return False


def letter2number(letter: str) -> int:
    r"""Parse letters 'a' through 'h' to the numbers 0 through 7.

    Parameters
    ----------
    Letter: str
        letter from 'a' to 'h'.

    Returns
    -------
    Integer that represents the letter passed as argument.
    """
    if letter == 'a':
        return 0
    if letter == 'b':
        return 1
    if letter == 'c':
        return 2
    if letter == 'd':
        return 3
    if letter == 'e':
        return 4
    if letter == 'f':
        return 5
    if letter == 'g':
        return 6
    if letter == 'h':
        return 7
    return None


def number2letter(number: int) -> str:
    r"""Parse numbers 0 through 7 to the letters a through h.

    Parameters
    ----------
    Number: int
        Number from 0 to 7.

    Returns
    -------
    String with the letter that represents the number passed as argument.

    """
    if number == 0:
        return 'a'
    if number == 1:
        return 'b'
    if number == 2:
        return 'c'
    if number == 3:
        return 'd'
    if number == 4:
        return 'e'
    if number == 5:
        return 'f'
    if number == 6:
        return 'g'
    if number == 7:
        return 'h'
    return None


def move_piece(x_0: int,
               y_0: int,
               x_1: int,
               y_1: int,
               board: list,
               white: bool,
               pgn
               ) -> str:
    r"""Attempts to move the piece in the (x_0, y_0) coordinate to the
    (x_1, y_1) coordinate.

    Parameters
    ----------
    x_0: int
        starting x coordinate
    y_0: int
        starting y coordinate.
    x_1: int
        finishing x coordinate.
    y_1: int
        finishing y coordinate.
    board: list
        list of Pieces.
    white: bool
        turn flag.
    pgn: PGN
        PGN object that handles the Portable Game Notation.

    Returns
    -------
    ‘moved’ if successful.

    ‘invalid’ if invalid move.

    ‘wrong color’ if piece specified isn’t of the right color.

    ‘not found’ if piece not found.
    """
    if [x_0, y_0, x_1, y_1] == [-4, -4, -4, -4]:
        pgn.add_move(None, white, x_0, y_0, x_1, y_1)
        return long_castle(board, white)
    if [x_0, y_0, x_1, y_1] == [-3, -3, -3, -3]:
        pgn.add_move(None, white, x_0, y_0, x_1, y_1)
        return short_castle(board, white)
    for i in board:
        if (x_0, y_0) == (i.x, i.y):
            if i.white is white:
                if i.move(x_1, y_1, board):
                    pgn.add_move(i.letter, white, x_0, y_0, x_1, y_1)
                    return 'moved'
                return 'invalid'
            return 'wrong color'
    return 'not found'


def check_check(board: list) -> list:
    r"""Checks if the arrangement of pieces passed as argument
    has the kings checked or not.

    Parameters
    ----------
    board: list
        list of Pieces

    Returns
    -------
    [flag1, flag2, flag3]

    Flag1: tuple or False
        tuple of the white king position, if he’s being checked.
        It is set to False otherwise.
    Flag2: tuple or False
        tuple of the black king position, if he’s being checked.
        It is set to False otherwise.
    Flag3: String or False
        String containing “white_check”, “black_check” or “both_check”,
        depending on the current situation.
        It is set to False if there are no checks occurring.
    """
    flag1 = False  # whitekingposition
    flag2 = False  # blackkingposition
    flag3 = False  # whitecheck / blackcheck / bothcheck
    for i in board:
        if isinstance(i, s.King):
            if i.white:
                white_king_position = (i.x, i.y)
            else:
                black_king_position = (i.x, i.y)
    for i in board:
        possible_attacks = i.get_possible_attacks(board)
        if i.white is False and white_king_position in possible_attacks:
            flag1 = white_king_position
            if flag3 == 'black_check':
                flag3 = 'both_check'
            elif flag3 is False:
                flag3 = 'white_check'
            # if white king is under attack
        if i.white and black_king_position in possible_attacks:
            flag2 = black_king_position
            if flag3 == 'white_check':
                flag3 = 'both_check'
            elif flag3 is False:
                flag3 = 'black_check'

            # if black king is under attack
    return [flag1, flag2, flag3]


# are there any moves the player can do?
def check_stalemate(board: list, white: bool) -> bool:
    r"""Checks if there are any moves the player in turn can do.

    Parameters
    ----------
    board: list
        list of Pieces
    white: bool
        turn flag

    Returns
    -------
    True if it is stalemate, False if it isn’t.
    """
    if len(board) == 2:
        return True
    test_board = copy.deepcopy(board)
    for _, piece in enumerate(test_board):
        if piece.white is not white:
            continue
        possible_moves = piece.get_possible_moves(test_board)
        for movimento in possible_moves:
            if piece.move(movimento[0], movimento[1], test_board):
                return False
            test_board = copy.deepcopy(board)

    # for i in range(len(test_board)):
    #     if test_board[i].white is not white:
    #         continue
    #     possible_moves = test_board[i].get_possible_moves(test_board)
    #     for movimento in possible_moves:
    #         if test_board[i].move(movimento[0], movimento[1], test_board):
    #             return False
    #         else:
    #             test_board = copy.deepcopy(board)
    return True


def print_board(board: list) -> None:
    r"""
Prints the board in the console, used for debugging.

    Parameters
    ----------
        board: list
            list of Pieces

    Returns
    -------
        Nothing.
    """
    for i in board:
        print(i, end=', ')
    print()


# check attacks in the 3 squares the king will have to go through in a castle
def check_attacks(board: list, squares: list, white: bool) -> bool:
    r"""Checks for attacks in the 3 squares the king will
    have to go through in a castle.

    Parameters
    ----------
    board: list
        list of Pieces

    squares: list
        list of tuples with coordinates to check

    white: bool
        turn flag

    Returns
    -------
    True if one of the squares are being attacked.

    False if they’re clear.
    """
    for i in board:
        if i.white == white:
            continue
        possible_attacks = i.get_possible_attacks(board)
        if any(square in possible_attacks for square in squares):
            return True
            # returns true if one of the squares are being attacked
    return False  # returns false if they're clear


def short_castle(board: list, white: bool) -> str:
    r"""Attempted to short castle.

    Parameters
    ----------
    board: list
        list of Pieces

    white: bool
        turn flag

    Returns
    -------
    ‘moved’ if successful,

    ‘invalid’ if move is invalid.
    """
    king = False
    rook = False
    doable = True
    if white:
        for i in board:
            if not i.white:
                continue
            if (i.x, i.y) == (4, 0) and isinstance(i, s.King) and i.moves == 0:
                king = i
            if (i.x, i.y) == (7, 0) and isinstance(i, s.Rook) and i.moves == 0:
                rook = i
            if (i.x, i.y) == (5, 0) or (i.x, i.y) == (6, 0):
                doable = False
        if not all([king, rook]) or not doable:
            return 'invalid'
        if check_attacks(board, [(4, 0), (5, 0), (6, 0)], white):
            return 'invalid'
        board.remove(king)
        board.remove(rook)
        board.append(s.King(6, 0, True))
        board.append(s.Rook(5, 0, True))
        return 'moved'
    # else
    for i in board:
        if i.white:
            continue
        if (i.x, i.y) == (4, 7) and isinstance(i, s.King) and i.moves == 0:
            king = i
        if (i.x, i.y) == (7, 7) and isinstance(i, s.Rook) and i.moves == 0:
            rook = i
        if (i.x, i.y) == (5, 7) or (i.x, i.y) == (6, 7):
            doable = False
    if not all([king, rook]) or not doable:
        return 'invalid'
    if check_attacks(board, [(4, 7), (5, 7), (6, 7)], white):
        return 'invalid'
    board.remove(king)
    board.remove(rook)
    board.append(s.King(6, 7, False))
    board.append(s.Rook(5, 7, False))
    return 'moved'


def long_castle(board: list, white: bool) -> str:
    r"""Attempted to long castle.

    Parameters
    ----------
    board: list
        list of Pieces.

    white: bool
        turn flag.

    Returns
    -------
    ‘moved’ if successful,

    ‘invalid’ if move is invalid.
    """
    king = False
    rook = False
    doable = True
    if white:
        for i in board:
            if not i.white:
                continue
            if (i.x, i.y) == (4, 0) and isinstance(i, s.King) and i.moves == 0:
                king = i
            if (i.x, i.y) == (0, 0) and isinstance(i, s.Rook) and i.moves == 0:
                rook = i
            if ((i.x, i.y) == (1, 0) or
                    (i.x, i.y) == (2, 0) or
                    (i.x, i.y) == (3, 0)):
                doable = False
        if not all([king, rook]) or not doable:
            return 'invalid'
        if check_attacks(board, [(4, 0), (2, 0), (3, 0)], white):
            return 'invalid'
        board.remove(king)
        board.remove(rook)
        board.append(s.King(2, 0, True))
        board.append(s.Rook(3, 0, True))
        return 'moved'
    # else
    for i in board:
        if i.white:
            continue
        if (i.x, i.y) == (4, 7) and isinstance(i, s.King) and i.moves == 0:
            king = i
        if (i.x, i.y) == (0, 7) and isinstance(i, s.Rook) and i.moves == 0:
            rook = i
        if ((i.x, i.y) == (1, 7) or
                (i.x, i.y) == (2, 7) or
                (i.x, i.y) == (3, 7)):
            doable = False
    if not all([king, rook]) or not doable:
        return 'invalid'
    if check_attacks(board, [(4, 7), (3, 7), (2, 7)], white):
        return 'invalid'
    board.remove(king)
    board.remove(rook)
    board.append(s.King(2, 7, False))
    board.append(s.Rook(3, 7, False))
    return 'moved'
