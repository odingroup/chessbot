"""
queen module
"""
from PIL import Image
from src import piece


class Queen(piece.Piece):
    """
    queen class
    """

    def __init__(self, x: int, y: int, white: bool) -> None:
        """
        queens init
        """
        super().__init__(x, y, white)
        self.letter = "Q"
        if white:
            self.img = Image.open('img/white_queen.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_queen.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get possible moves
        """
        self.possible_moves = []

        i = self.x
        while i < 7:  # checking moves to the right
            i = i + 1
            test = self.check_for_piece_color(board, i, self.y)
            if test == 'empty':
                self.possible_moves.append((i, self.y))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, self.y))
                break

        i = self.x
        while i > 0:  # checking moves to the left
            i = i - 1
            test = self.check_for_piece_color(board, i, self.y)
            if test == 'empty':
                self.possible_moves.append((i, self.y))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, self.y))
                break

        i = self.y
        while i < 7:  # checking moves up
            i = i + 1
            test = self.check_for_piece_color(board, self.x, i)
            if test == 'empty':
                self.possible_moves.append((self.x, i))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((self.x, i))
                break

        i = self.y
        while i > 0:  # checking moves down
            i = i - 1
            test = self.check_for_piece_color(board, self.x, i)
            if test == 'empty':
                self.possible_moves.append((self.x, i))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((self.x, i))
                break

        i = self.x
        j = self.y
        while i < 7 and j < 7:
            i = i + 1  # up right diagonal
            j = j + 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i > 0 and j < 7:
            i = i - 1  # up left diagonal
            j = j + 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i < 7 and j > 0:
            i = i + 1  # down right diagonal
            j = j - 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i > 0 and j > 0:
            i = i - 1  # down left diagonal
            j = j - 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break
        return self.possible_moves

    def get_possible_attacks(self, board: list) -> list:
        """
        get possible attacks
        """
        return self.get_possible_moves(board)
