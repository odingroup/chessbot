"""
piece module
"""
from src import utils


class Piece:
    """generic piece class"""

    def __init__(self, x_coord: int, y_coord: int, white: bool) -> None:
        """
        piece init
        """
        self.x = x_coord
        self.y = y_coord
        self.just_moved2 = False  # only used for pawns
        self.white = white
        self.possible_moves = []
        self.possible_attacks = []

    def __str__(self) -> str:
        """
        piece str parser
        """
        color = 'White' if self.white else 'Black'
        return f'{color} {self.__class__.__name__} in ({self.x},{self.y})'

    def check_for_piece_color(self,
                              chess_board: list,
                              x_coord: int,
                              y_coord: int):
        """
        check for piece color
        """
        for i in chess_board:
            if (i.x, i.y) == (x_coord, y_coord):
                return i.white
        return 'empty'

    def get_possible_moves(self, _) -> list:
        """
        get possible moves, overwritten in other piece classes
        """

    def move(self, x_coord: int, y_coord: int, chess_board: list) -> bool:
        """
        piece move
        """
        if (x_coord, y_coord) in self.get_possible_moves(chess_board):
            oldx = self.x
            oldy = self.y
            old_piece = False
            for i in chess_board:
                if (i.x, i.y) == (x_coord, y_coord):
                    if i.white is not self.white:
                        chess_board.remove(i)
                        old_piece = i
                if i.just_moved2:
                    i.just_moved2 = False
            self.x = x_coord
            self.y = y_coord
            [_, _, check] = utils.check_check(chess_board)
            # if at the end of whites move, there's a check on whites king,
            # go back to how it was before
            if (check == 'both_check' or
                    (check == 'white_check' and self.white) or
                    (check == 'black_check' and not self.white)):
                if old_piece:
                    chess_board.append(old_piece)
                self.x = oldx
                self.y = oldy
                return False
            return True
        return False
