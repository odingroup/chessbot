"""
king module
"""

from PIL import Image
from src import piece


class King(piece.Piece):
    """
    King class
    """

    def __init__(self, x_coord: int, y_coord: int, white: bool) -> None:
        """
        Kings init
        """
        super().__init__(x_coord, y_coord, white)
        self.letter = 'K'
        self.moves = 0
        self.possible_moves = []
        self.possible_attacks = []
        if white:
            self.img = Image.open('img/white_king.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_king.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get Kings possible moves
        """
        self.possible_moves = []
        for d_i in [0, 1, -1]:
            for d_j in [0, 1, -1]:
                if d_j == 0 and d_i == 0:
                    continue
                i = self.x + d_i
                j = self.y + d_j
                if (0 <= i <= 7) and (0 <= j <= 7):
                    test = self.check_for_piece_color(board, i, j)
                    if test == 'empty':
                        self.possible_moves.append((i, j))
                    elif test == self.white:
                        continue
                    else:
                        self.possible_moves.append((i, j))
        return self.possible_moves

    def get_possible_moves2(self, board: list) -> list:
        """
        get Kings possible moves2
        """
        self.possible_moves = []
        for d_i in [0, 1, -1]:
            for d_j in [0, 1, -1]:
                if d_j == 0 and d_i == 0:
                    continue
                i = self.x + d_i
                j = self.y + d_j
                if (0 <= i <= 7) and (0 <= j <= 7):
                    test = self.check_for_piece_color(board, i, j)
                    if test == 'empty':
                        self.possible_moves.append((i, j))
                    elif test == self.white:
                        continue
                    else:
                        self.possible_moves.append((i, j))
        return self.possible_moves

    def get_possible_attacks(self, _) -> list:
        """
        get Kings possible attacks
        """
        possible_attacks = []
        for d_i in [0, 1, -1]:
            for d_j in [0, 1, -1]:
                i = self.x + d_i
                j = self.y + d_j
                if (0 <= i <= 7) and (0 <= j <= 7):
                    possible_attacks.append((i, j))
        possible_attacks.remove((self.x, self.y))
        return possible_attacks

    def move(self, x_coord: int, y_coord: int, chess_board: list) -> bool:
        """
        King move function
        """
        if super().move(x_coord, y_coord, chess_board):
            self.moves += 1
            return True
        return False
