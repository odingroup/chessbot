"""
bishop module
"""

from PIL import Image
from src import piece


class Bishop(piece.Piece):
    """
    Bishop class
    """

    def __init__(self, x_coord: int, y_coord: int, white: bool) -> None:
        """
        Bishops init
        """
        super().__init__(x_coord, y_coord, white)
        self.letter = 'B'
        self.possible_moves = []
        self.possible_attacks = []
        if white:
            self.img = Image.open('img/white_bishop.png').convert('RGBA')
        else:
            self.img = Image.open('img/black_bishop.png').convert('RGBA')

    def get_possible_moves(self, board: list) -> list:
        """
        get Bishops possible moves
        """
        self.possible_moves = []

        i = self.x
        j = self.y
        while i < 7 and j < 7:
            i = i + 1  # diagonal superior direita
            j = j + 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i > 0 and j < 7:
            i = i - 1  # diagonal superior esquerda
            j = j + 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i < 7 and j > 0:
            i = i + 1  # diagonal inferior direita
            j = j - 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break

        i = self.x
        j = self.y
        while i > 0 and j > 0:
            i = i - 1  # diagonal inferior esquerda
            j = j - 1
            test = self.check_for_piece_color(board, i, j)
            if test == 'empty':
                self.possible_moves.append((i, j))
            elif test == self.white:
                break
            else:
                self.possible_moves.append((i, j))
                break
        return self.possible_moves

    def get_possible_attacks(self, board: list) -> list:
        """
        get Bishops possible attacks
        """
        return self.get_possible_moves(board)
