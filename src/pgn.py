"""
PGN module
"""

from datetime import date
import requests
from src import utils


class PGN:
    """Portable Game Notation of the match"""

    def __init__(self, name_white: str, name_black: str) -> None:
        """
        PGNs init
        """
        day = date.today()
        self.notation = f'[Event "Chess Bot match"]\n\
                [Date "{day}"]\n[White "{name_white}"]\n\
                [Black "{name_black}"]\n'
        self.round = 1

    def add_move(self,
                 letter: str,
                 white: bool,
                 x_0: int,
                 y_0: int,
                 x_1: int,
                 y_1: int) -> None:
        """
        add move function
        """
        if white:
            self.notation += f"{self.round}. "
            self.round += 1
        if [x_0, y_0, x_1, y_1] == [-4, -4, -4, -4]:  # long castle
            self.notation += "0-0-0 "
            return
        if [x_0, y_0, x_1, y_1] == [-3, -3, -3, -3]:  # short castle
            self.notation += "0-0 "
            return
        self.notation += f"{letter}{utils.number2letter(x_0)}{y_0+1}\
                           {utils.number2letter(x_1)}{y_1+1} "

    def end_game(self, result: str) -> None:
        """
        add end game notation
        """
        if result == 'White':
            self.notation += "1-0"
        elif result == 'Black':
            self.notation += "0-1"
        elif result == 'draw':
            self.notation += "1/2-1/2"
        self.notation += "\n"

    def get_link_analise(self):
        """
        get link from lichess.org
        """
        url_base = 'https://lichess.org'
        url = url_base+"/import"
        pgn = {'pgn': self.notation}
        req = requests.post(url, pgn, allow_redirects=False)
        return url_base+req.headers['location']
