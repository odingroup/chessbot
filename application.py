"""
documentaria
"""

import os
import asyncio
import random
import discord
from dotenv import load_dotenv
from discord.ext import commands

from src import utils
from src.pgn import PGN

load_dotenv()
BOT_KEY = os.getenv("BOT_KEY")
client = commands.Bot(command_prefix="-")

MATCH_ID = 0
MOVE_TIMEOUT = 86400  # 24 hour moves
VERSION = "2.1.0"


@client.event
async def on_ready():
    r"""
    on ready event
    """
    print("We have logged in as {0.user}".format(client))
    await client.change_presence(
        activity=discord.Game(name=f"-chess v: {VERSION}"))


@client.event
async def on_message(message: discord.Message):
    """
    on message event
    """
    if message.author == client.user:
        return
    if message.content.lower() in ('-forfeit', '-draw'):
        return
    await client.process_commands(message)


@client.command()
async def ping(ctx):
    """
    ping command
    shows version
    """
    await ctx.channel.send(f"pong\nversion {VERSION}")


@client.command()
async def chess(ctx, member: discord.Member = None):
    """
    chess command
    """
    if member is None:
        await ctx.send("You need someone to play with")
    else:
        await ctx.send(
            f"{member.mention}, do you want to play Chess? (yes/no)"
        )

        def check1(message):
            return (message.content.lower() in ("yes", "y", "n", "no") and
                    message.author == member)

        try:
            msg = await client.wait_for("message", check=check1, timeout=60.0)
        except asyncio.TimeoutError:
            await ctx.message.add_reaction("😴")
            await ctx.send(f"{member.mention} took too long to respond.")
            return

        if msg.content.lower() in ("yes", "y"):  # match will start
            global MOVE_TIMEOUT                          # time limit for moves
            global MATCH_ID                              # global id tracker
            current_match = MATCH_ID                     # id of this match
            players = [member, ctx.author]               # list of players
            random.shuffle(players)                      # shuffle order
            turn = players[0]                            # select who starts
            pgn = PGN(str(players[0]), str(players[1]))  # initialize pgn
            turn_white = True                            # white starts
            red_square = False                           # check flag
            board = utils.initialize_board()             # board instance
            utils.get_board(board, turn_white, current_match, red_square)
            MATCH_ID += 1

            def check2(message):  # function that checks for move
                return ((message.author.id == turn.id) and
                        message.channel == ctx.channel and
                        (utils.parse_command(message.content)))
            await ctx.send(START_INSTRUCTIONS)

            # game loop
            while True:
                turn_color = "White" if turn_white else "Black"
                await ctx.send(
                    f"{turn.mention}'s turn - {turn_color} moves!",
                    file=discord.File(str(current_match) + ".png"),
                )
                try:
                    msg = await client.wait_for(
                        "message",
                        check=check2,
                        timeout=MOVE_TIMEOUT)
                except asyncio.TimeoutError:
                    await ctx.send("Player took too long to respond")
                    winner = "White" if turn_color == "Black" else "Black"
                    await end_game(
                        current_match,
                        ctx,
                        result=winner,
                        pgn=pgn)
                    break
                command = utils.parse_command(msg.content)
                if command:
                    [x_0, y_0, x_1, y_1] = command
                    if [x_0, y_0, x_1, y_1] == [-2, -2, -2, -2]:
                        # propose draw
                        other_player = (
                            players[0] if turn == players[1] else players[1])
                        await ctx.send(
                            f"**{turn.name}** proposes a draw." +
                            f"{other_player.mention}" +
                            "do you accept? (yes/no)")
                        try:
                            msg2 = await client.wait_for("message",
                                                         check=check1,
                                                         timeout=60.0)
                        except asyncio.TimeoutError:
                            await msg.add_reaction("😴")
                            continue
                        if msg2.content == "yes":
                            await ctx.send("Match ends in a draw! 🙊")
                            await end_game(current_match,
                                           ctx,
                                           result='draw',
                                           pgn=pgn
                                           )
                            break
                        if msg2.content == "no":
                            await ctx.send("The match will procede")
                    elif [x_0, y_0, x_1, y_1] == [-1, -1, -1, -1]:  # forfeit
                        winner = "White" if turn_color == "Black" else "Black"
                        await ctx.send(f"{turn_color} forfeits," +
                                       f"**{winner}** wins! 🎉")
                        await end_game(current_match,
                                       ctx,
                                       result=winner,
                                       pgn=pgn)
                        break

                    # actual movement
                    movimento = utils.move_piece(
                        x_0, y_0, x_1, y_1, board, turn_white, pgn)
                    if movimento == "moved":
                        [position_white, position_black,
                            check] = utils.check_check(board)
                        # paint the square red
                        red_square = (position_white if check ==
                                      "white_check" else position_black)
                        # update board image
                        utils.get_board(
                            board,
                            not turn_white,
                            current_match,
                            red_square,
                            (x_0, y_0),
                            (x_1, y_1))
                        if check in ("white_check", "black_check"):
                            # if king is under attack
                            if utils.check_stalemate(board, not turn_white):
                                await ctx.send(
                                    f"Checkmate! {turn_color} wins! 🎉",
                                    file=discord.File(
                                        str(current_match) + ".png"),
                                )
                                await end_game(current_match,
                                               ctx,
                                               result=turn_color,
                                               pgn=pgn)
                                break
                            await ctx.send("**CHECK!**")
                        if utils.check_stalemate(board, not turn_white):
                            await ctx.send(
                                "**Stalemate!** Nobody wins 🤷‍♂️",
                                file=discord.File(str(current_match) + ".png"),
                            )
                            await end_game(current_match,
                                           ctx,
                                           result='draw',
                                           pgn=pgn
                                           )
                            break
                        turn = players[0] if turn == players[1] else players[1]
                        turn_white = not turn_white
                        # False if turn_white else True
                    elif movimento == "wrong color":
                        await ctx.send("this piece is not yours")
                    elif movimento == "invalid":
                        await ctx.send("invalid move")
                    elif movimento == "not found":
                        await ctx.send("piece not found")

        elif msg.content.lower() in ("no", "n"):
            await ctx.send("ok 😔")


async def end_game(current_match,
                   ctx,
                   result=None,
                   pgn=None):
    """
    end game function
    """
    if pgn is not None:
        pgn.end_game(result)
        await ctx.send("To see an analysis of the game go to " +
                       pgn.get_link_analise())
    if os.path.exists(str(current_match) + ".png"):
        os.remove(str(current_match) + ".png")

START_INSTRUCTIONS = "```m\n" + \
    "- Send a message here with the position of the piece " + \
    "you want to move and where you want it to move.\n\n" + \
    "Example: for moving the pawn from d2 to d4 you send \"d2d4\"\n\n" + \
    "- For castling send either \"0-0\" or \"0-0-0\"," + \
    "for castling short or long, respectively\n\n" + \
    "- For forfeiting send \"-forfeit\" and " + \
    "for proposing a draw \"-draw\"\n" + \
    "```"

if __name__ == "__main__":
    client.run(BOT_KEY)
